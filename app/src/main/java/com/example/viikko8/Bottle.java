package com.example.viikko8;



public class Bottle {
    String name;
    double size;
    double price;

    public Bottle(String new_name, double new_size, double new_price) {
        name = new_name;
        size = new_size;
        price = new_price;
    }

    public String getName(){
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getSize() {
        return size;
    }
}
