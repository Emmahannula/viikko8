package com.example.viikko8;

import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class BottleDispenser {
    private int bottles;
    private double money;
    ArrayList<Bottle> bottlelist = new ArrayList<>();

    private static BottleDispenser BD = new BottleDispenser();

    private BottleDispenser() {
        bottles = 6;
        money = 0;

        bottlelist.add(new Bottle("Pepsi Max", 0.5, 1.8));
        bottlelist.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottlelist.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        bottlelist.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottlelist.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottlelist.add(new Bottle("Fanta Zero", 1.5, 2.5));
    }

    public static BottleDispenser getDispenser() {
        return BD;
    }


    public void addMoney(TextView text, double amount) {
        money += amount;
        text.setText("Klink! Rahaa lisätty laitteeseen!\n");
        text.append("Rahaa koneessa " + money + "€");
    }

    public void buyBottle(TextView text, String valinta) {
        double price;
        int indeksi=-1;

        for (int i = 0; i<bottlelist.size(); i++) {
            String pullo = (bottlelist.get(i).getName() + " Koko: " + bottlelist.get(i).getSize() + " Hinta: "+ bottlelist.get(i).getPrice());
            if (pullo.equals(valinta)) {
                indeksi = i;
            }
        }

        if (indeksi == -1) {
            text.setText("Haluamaasi pulloa ei ole, valitse toinen pullo.");
        }
        else {

            price = bottlelist.get(indeksi).getPrice();

            if (bottles == 0) {
                text.setText("Ei pulloja");
            } else if (money == 0 || price > money) {
                text.setText("Syötä rahaa ensin!");
            } else {
                bottles -= 1;
                money -= price;
                String name = bottlelist.get(indeksi).getName();
                text.setText("KACHUNK! " + name + " tipahti masiinasta!");

                if (bottlelist.size() > 0)
                    bottlelist.remove(indeksi);
            }
        }
    }

    public void returnMoney(TextView text) {
        DecimalFormat df = new DecimalFormat("0.00");
        text.setText("Klink klink. Sinne menivät rahat!\n Rahaa tuli ulos "+df.format(money) + "€");
        money = 0;
    }

    public void printBottles(TextView text) {

        text.setText(null);
        for (int i = 0; i<bottlelist.size(); i++) {
            text.setText(text.getText().toString()+(i+1) + ". Nimi: "+ bottlelist.get(i).getName()+"\n");
            text.setText(text.getText().toString()+"    Koko: "+bottlelist.get(i).getSize()+"\tHinta: "+bottlelist.get(i).getPrice()+"\n");
        }
    }

    public ArrayList<String> createList() {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Valittavat pullot:");
        for (int i = 0; i<bottlelist.size(); i++) {
            lista.add(bottlelist.get(i).getName() + " Koko: " + bottlelist.get(i).getSize() + " Hinta: "+ bottlelist.get(i).getPrice());
        }
        return lista;
    }

}