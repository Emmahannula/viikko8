package com.example.viikko8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    BottleDispenser pullo = BottleDispenser.getDispenser();
    SeekBar seekBar;
    TextView text;
    TextView seekBarValue;
    Spinner spinner;
    double money;
    ArrayList<String> lista;
    String valinta;
    File file;
    String filename = "kuitti.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.textView);
        seekBar=(SeekBar) findViewById(R.id.seekBar);
        seekBarValue = (TextView) findViewById(R.id.seekBarValue);
        seekBar.setMax(4);
        //file = new File(getExternalFilesDir(null), filename);

        addToSpinner();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                money = progress;
                seekBarValue.setText(progress+ "€");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public void addToSpinner() {
        lista = pullo.createList();
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void add(View v) {
        pullo.addMoney(text, money);
        seekBar.setProgress(0);
    }

    public void buy(View v) {
        valinta = String.valueOf(spinner.getSelectedItem());
        pullo.buyBottle(text, valinta);
    }
    public void returnM(View v) {
        pullo.returnMoney(text);
    }

    public void print(View v) {
        pullo.printBottles(text);
    }

    public void tulostaKuitti(View v) throws IOException {
        try {
            FileOutputStream outputStream;
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write("***Kuitti***\n".getBytes());
            outputStream.write(valinta.getBytes());
            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        text.setText("Kuitti tulostettu tiedostoon kuitti.txt");
    }

    public void readFileInternalStorage(View view) {
        try {
            FileInputStream fileInputStream = openFileInput(filename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));

            StringBuffer sb = new StringBuffer();
            String line = reader.readLine();

            while (line != null) {
                sb.append(line);
                line = reader.readLine();
            }
            text.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


